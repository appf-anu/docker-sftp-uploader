FROM alpine:3.12
RUN apk add --no-cache tini bash openssh-client findutils sshpass sed inotify-tools jq

WORKDIR /app

COPY mustache* /usr/bin/
RUN ln -s "/usr/bin/mustache-cli-linux-$(arch)" /usr/bin/mustache-cli

ENV SFTP_HOST "sftp-server"
ENV SFTP_PORT "22"
ENV SFTP_USER "upload"
ENV SFTP_PASSWORD ""
ENV UPLOAD_PREFIX "upload/"

COPY sftp.job.tpl .
COPY entrypoint.sh /
ENTRYPOINT ["/sbin/tini", "--", "/entrypoint.sh"]