#!/bin/bash

export SSHPASS="${SFTP_PASSWORD}"

# sshpass -e sftp -oStrictHostKeyChecking=accept-new -b -
# 
#  mount directory is /data

function parentlist (){
    dval="$1"
    dval=$(echo "/$dval"|tr -s "/")
    rval=""
    dir=$(dirname "$dval")
    while [ "$dir" != "/" ]; do
        rval="${rval}$dir\n"
        dir=$(dirname "$dir")
    done
    echo $rval
}

addStringToTemplateVars(){
    echo $(echo $1 | \
            jq --arg value "$2" --arg key "$3" '. + {($key): $value}'
          )
}

addJSONToTemplateVars(){
    echo $(echo $1 | \
            jq --argjson value "$2" --arg key "$3" '. + {($key): $value}'
          )
}


uploadSFTP(){
    # tell us why
    echo "action: $2"
    mainJSON="{}"
    # set the local file name and add it to template variables
    local_fn="$1"
    mainJSON=$(addStringToTemplateVars "$mainJSON" "$local_fn" "local_fn")

    remote_fn="${UPLOAD_PREFIX%%/}/"$(echo "$local_fn" |sed "s:^\/data/::")
    remote_fn=$(echo "$remote_fn"|tr -s "/")
    mainJSON=$(addStringToTemplateVars "$mainJSON" "$remote_fn" "remote_fn")

    parent_list=$(parentlist "$remote_fn")
    remote_dirs=$(echo $parent_list | \
        jq --raw-input 'split("\\n") | map(select(. != ""))|reverse|.[1:]')
    mainJSON=$(addJSONToTemplateVars "$mainJSON" "$remote_dirs" "remote_dirs")

    remote_dir=$(dirname "$remote_fn")
    temp_fn="$remote_dir/.$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 8).tmp"
    mainJSON=$(addStringToTemplateVars "$mainJSON" "$temp_fn" "temp_fn")


    echo $mainJSON | jq > vars.json
    mustache-cli vars.json sftp.job.tpl > /tmp/sftp.job

    if [[ ! -z "${DEBUG}" ]]; then
        echo $mainJSON | jq
        echo "uploading $local_fn to sftp://$SFTP_USER:$SSHPASS@$SFTP_HOST:$SFTP_PORT/$remote_fn"
        cat /tmp/sftp.job
    else
        echo "uploading $local_fn to sftp://$SFTP_USER:$SSHPASS@$SFTP_HOST:$SFTP_PORT/$remote_fn"
    fi

    sshpass -e sftp -r -oStrictHostKeyChecking=accept-new -oBatchMode=no -b /tmp/sftp.job -P $SFTP_PORT "$SFTP_USER@$SFTP_HOST" 2>/tmp/stderr.out

    retval=$?
    if [ $retval -eq 0 ]; then
        echo "uploaded $1 to ${remote_fn}"
        if [[ -z "${NO_DELETE}" ]]; then
            rm -r "${local_fn}"
            echo "removed ${local_fn}"
        fi
        rm /tmp/stderr.out
        return 0
    else
        echo "something went wrong uploading ${local_fn} to ${remote_fn}"
        echo "$(cat /tmp/stderr.out)"
        rm /tmp/stderr.out
        return $retval
    fi
}

echo "startup, uploading existing files..."
while IFS='' read -r -d '' filename; do
  until uploadSFTP "$filename" "startup"; do
    echo "sleeping for 30 seconds for initial upload"
    sleep 30
  done
done < <(find /data -not -path '*/\.*' -type f -print0)
find /data/* -type d -empty -delete


inotifywait -r -m /data \
    --event close_write \
    --event moved_to \
    --exclude '/\.' \
    | while read path action file; do
        if [[ -d "${path}${file}" ]]; then
            while IFS='' read -r -d '' filename; do
              until uploadSFTP "$filename" "startup"; do
                echo "sleeping for 30 seconds for $action ${path}${file}"
                sleep 30
              done
            done < <(find "${path}${file}" -not -path '*/\.*' -type f -print0)
        else
            until uploadSFTP "${path}${file}" "$action"; do
               echo "sleeping for 30 seconds for $action ${path}${file}"
               sleep 30
            done
        fi
        find /data/* -type d -empty -delete
    done